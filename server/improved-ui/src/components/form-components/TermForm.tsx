import React, { useContext } from 'react';
import { Formik, Form, FormikHelpers } from 'formik';
import Add from '@material-ui/icons/Add';
import { v4 } from 'uuid';
import CenterButton from '../layout/CenterButton';
import { AppContext } from '../../context/AppContext';
import TermInput from './TermInput';
import { TermSchema } from '../../schemas/termSchema';

interface TermFormProps {
  term: string;
}

const TermForm: React.FC = () => {
  const { addTerm } = useContext(AppContext);
  const initialValues: TermFormProps = { term: '' };

  const submitHandler: (
    data: TermFormProps,
    helpers: FormikHelpers<TermFormProps>
  ) => void = ({ term }, { setSubmitting, resetForm }) => {
    setSubmitting(true);
    addTerm!({ id: v4(), name: term });
    setSubmitting(false);
    resetForm();
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={TermSchema}
      onSubmit={submitHandler}>
      {({ handleChange, handleBlur }) => (
        <>
          <Form>
            <TermInput
              onChange={handleChange}
              onBlur={handleBlur}
              label='Digite um termo'
              type='text'
              name='term'
            />
            <CenterButton
              type='submit'
              variant='contained'
              size='large'
              startIcon={<Add />}
              color='primary'>
              Adicionar termo
            </CenterButton>
          </Form>
        </>
      )}
    </Formik>
  );
};

export default TermForm;

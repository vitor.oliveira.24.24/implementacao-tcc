import React from 'react';
import { FieldAttributes, useField } from 'formik';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';

type TermInputProps = {
  label: string;
  type: string;
  defaultValue?: string | number;
} & FieldAttributes<{}>;

const TermInput: React.FC<TermInputProps> = ({
  label,
  type,
  defaultValue,
  ...props
}) => {
  const [field, meta] = useField(props);
  const errorInfo = meta.touched && meta.error ? meta.error : '';

  return (
    <FormControl fullWidth>
      <TextField
        {...field}
        label={label}
        helperText={errorInfo}
        error={!!errorInfo}
        type={type}
        margin='normal'
        defaultValue={defaultValue ?? null}
        fullWidth
      />
    </FormControl>
  );
};

export default TermInput;

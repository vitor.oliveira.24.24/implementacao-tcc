import React, { useContext, useState, useEffect } from 'react';
import { Formik, Form, FormikHelpers } from 'formik';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import Save from '@material-ui/icons/Save';
import Settings from '@material-ui/icons/Settings';
import { ConfigSchema } from '../../schemas/configSchema';
import { ConfigContext, ToolConfig } from '../../context/ConfigContext';
import TermInput from './TermInput';
import CenterButton from '../layout/CenterButton';
import CustomAlert from '../layout/CustomAlert';

const ConfigForm: React.FC = () => {
  const { config, updateConfig } = useContext(ConfigContext);
  const [saved, setSaved] = useState(false);
  let initialValues: ToolConfig = config;

  useEffect(() => {
    if (saved) {
      setTimeout(() => setSaved(false), 2500);
    }
  }, [saved]);

  const submitHandler: (
    data: ToolConfig,
    helpers: FormikHelpers<ToolConfig>
  ) => void = (data, { setSubmitting, resetForm }) => {
    setSubmitting(true);
    updateConfig!(data);
    setSaved(true);
    setSubmitting(false);
    resetForm();
  };

  return (
    <Formik
      initialValues={initialValues}
      validationSchema={ConfigSchema}
      onSubmit={submitHandler}>
      {({ values, handleChange, handleBlur }) => (
        <>
          <Typography
            style={{ marginTop: '1%' }}
            variant='h4'
            align='center'
            gutterBottom>
            <span>
              <Settings />
            </span>{' '}
            Menu de configurações da ferramenta
          </Typography>
          <Form style={{ marginTop: '2%' }}>
            <FormControl fullWidth>
              <InputLabel htmlFor='lang'>Linguagem</InputLabel>
              <Select
                inputProps={{
                  id: 'lang',
                  name: 'lang',
                }}
                defaultValue={values.lang}
                onChange={handleChange}
                onBlur={handleBlur}>
                <MenuItem value='english' selected>
                  Inglês
                </MenuItem>
                <MenuItem value='portuguese'>Português</MenuItem>
              </Select>
            </FormControl>
            <TermInput
              onChange={handleChange}
              onBlur={handleBlur}
              name='pages'
              type='number'
              defaultValue={values.pages}
              label='Quantidade de páginas'
            />

            <CenterButton
              type='submit'
              variant='contained'
              size='large'
              startIcon={<Save />}
              color='primary'>
              Salvar Configurações
            </CenterButton>
          </Form>
          {saved ? (
            <CustomAlert severity='success' style={{ marginTop: '2%' }}>
              Configuração salva com sucesso !!!
            </CustomAlert>
          ) : (
            <></>
          )}
        </>
      )}
    </Formik>
  );
};

export default ConfigForm;

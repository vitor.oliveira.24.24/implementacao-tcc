import ListItemText from '@material-ui/core/ListItemText';
import Delete from '@material-ui/icons/Delete';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import React, { useContext } from 'react';
import { Term, AppContext } from '../../context/AppContext';

interface ListItemProps {
  term: Term;
}

const termItemStyle = makeStyles((theme: Theme) =>
  createStyles({
    deleteBtn: {
      cursor: 'pointer',
    },
  })
);

const TermListItem: React.FC<ListItemProps> = ({ term }) => {
  const { delTerm } = useContext(AppContext);
  const { id, name } = term;
  const { deleteBtn } = termItemStyle();

  return (
    <ListItem divider>
      <ListItemText primary={name} inset />
      <ListItemIcon>
        <Delete
          className={deleteBtn}
          color='error'
          onClick={() => delTerm!(id)}
        />
      </ListItemIcon>
    </ListItem>
  );
};

export default TermListItem;

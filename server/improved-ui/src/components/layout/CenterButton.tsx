import React from 'react';
import Button, { ButtonProps } from '@material-ui/core/Button';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

const centerBtnStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      justifyContent: 'center',
      marginTop: '1%',
    },
    btn: {
      padding: '1em',
    },
  })
);

const CenterButton: React.FC<ButtonProps> = ({ ...props }) => {
  const { root, btn } = centerBtnStyles();
  return (
    <div className={root}>
      <Button className={btn} {...props}>
        {props.children}
      </Button>
    </div>
  );
};

export default CenterButton;

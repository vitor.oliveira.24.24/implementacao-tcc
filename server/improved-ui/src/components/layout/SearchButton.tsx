import React, { useContext, useState, useEffect } from 'react';
import axios from 'axios';
import CircularProgress from '@material-ui/core/CircularProgress';
import Search from '@material-ui/icons/Search';
import Error from '@material-ui/icons/Error';
import { v4 } from 'uuid';
import { AppContext } from '../../context/AppContext';
import { ConfigContext } from '../../context/ConfigContext';
import CenterButton from './CenterButton';
import CustomAlert from './CustomAlert';

interface ErrorState {
  hasError: boolean;
  message: string;
}

const SearchButton: React.FC = () => {
  const [errorStatus, setErrorStatus] = useState<ErrorState>({
    hasError: false,
    message: '',
  });
  const [isLoading, setLoading] = useState(false);
  const { config } = useContext(ConfigContext);
  const { terms, addTerm } = useContext(AppContext);
  const { lang, pages } = config;
  const termList = terms.map(({ name }) => name);

  useEffect(() => {
    if (errorStatus) {
      setTimeout(() => setErrorStatus({ hasError: false, message: '' }), 2500);
    }
  }, [errorStatus]);

  const search = async () => {
    try {
      setLoading(true);
      const { data } = await axios.post('/search', {
        termList,
        pages,
        lang,
      });

      const { recommendations } = data;
      recommendations.forEach((name: string) => addTerm!({ id: v4(), name }));
    } catch ({ message }) {
      setErrorStatus({ hasError: true, message });
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      <CenterButton
        type='submit'
        variant='contained'
        size='large'
        startIcon={<Search />}
        onClick={search}
        color='primary'>
        Pesquisar termos.
      </CenterButton>
      {errorStatus.hasError ? (
        <CustomAlert icon={<Error />} severity='error'>
          {errorStatus.message}
        </CustomAlert>
      ) : (
        <></>
      )}
      {isLoading ? (
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            marginTop: '5%',
          }}>
          <CircularProgress size='5rem' />
        </div>
      ) : (
        <></>
      )}
    </>
  );
};

export default SearchButton;

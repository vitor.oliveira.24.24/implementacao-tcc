import React, { useContext, useEffect, useState } from 'react';
import List from '@material-ui/core/List';
import Typography from '@material-ui/core/Typography';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { AppContext } from '../../context/AppContext';
import TermListItem from './TermListItem';
import SearchButton from './SearchButton';

const termListStyles = makeStyles((theme: Theme) =>
  createStyles({
    listTitle: {
      marginTop: '2%',
    },
    emptyListStyle: {
      marginTop: '5%',
    },
  })
);

const TermList: React.FC = () => {
  const { terms } = useContext(AppContext);
  const [len, setLen] = useState(0);
  const { emptyListStyle, listTitle } = termListStyles();

  useEffect(() => {
    setLen(terms.length);
  }, [terms]);

  if (len > 0) {
    return (
      <div>
        <Typography
          className={listTitle}
          variant='h4'
          align='center'
          color='primary'
          gutterBottom>
          Lista de Termos
        </Typography>
        <List>
          {terms.map((term, index) => (
            <TermListItem key={index} term={term} />
          ))}
        </List>
        <SearchButton />
      </div>
    );
  }

  return (
    <div>
      <Typography className={emptyListStyle} variant='h4' align='center'>
        <span>
          <SentimentVeryDissatisfiedIcon color='error' />
        </span>{' '}
        Não existem termos.
      </Typography>
    </div>
  );
};

export default TermList;

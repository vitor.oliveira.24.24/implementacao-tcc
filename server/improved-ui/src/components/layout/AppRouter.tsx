import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link as RouterLink,
} from 'react-router-dom';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';
import IconButton from '@material-ui/core/IconButton';
import Home from '@material-ui/icons/Home';
import Typography from '@material-ui/core/Typography';
import About from '../../routes/About';
import Config from '../../routes/Config';
import MainRoute from '../../routes/MainRoute';

const routerStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
    },
    homeBtn: {
      marginRight: theme.spacing(2),
    },
  })
);

const AppRouter: React.FC = (props) => {
  const { root, homeBtn } = routerStyles();

  return (
    <div className={root}>
      <CssBaseline />
      <Router>
        <AppBar position='static'>
          <Toolbar>
            <Link
              color='inherit'
              underline='none'
              component={RouterLink}
              to='/'>
              <IconButton
                className={homeBtn}
                edge='start'
                color='inherit'
                aria-label='menu'>
                <Home />
              </IconButton>
            </Link>
            <Typography variant='h6' style={{ padding: '0 1em 0 0' }}>
              <Link color='inherit' to='/config' component={RouterLink}>
                Configurações
              </Link>
            </Typography>
            {/* <Typography variant='h6'>
              <Link color='inherit' to='/about' component={RouterLink}>
                Informações
              </Link>
            </Typography> */}
          </Toolbar>
        </AppBar>
        <Switch>
          <Route path='/' component={MainRoute} exact />
          <Route path='/config' component={Config} />
          {/* <Route path='/about' component={About} /> */}
        </Switch>
      </Router>
    </div>
  );
};

export default AppRouter;

import React from 'react';

interface IDebugFormProps {
    values: Object;
}

const DebugForm: React.FC<IDebugFormProps> = ({ values }) => {
    return (
        <pre style={{ marginTop: '5%' }}>
            {JSON.stringify(values, null, 2)}
        </pre>
    );
}

export default DebugForm;

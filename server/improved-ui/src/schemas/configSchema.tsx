import * as Yup from 'yup';

export const ConfigSchema = Yup.object().shape({
  pages: Yup.number()
    .integer('precisa ser um número inteiro')
    .min(0, 'Quantidade de páginas maior ou igual a zero')
    .max(100, 'Quantidade de páginas não permitida')
    .optional(),
});

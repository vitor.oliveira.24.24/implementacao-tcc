import * as Yup from 'yup';

export const TermSchema = Yup.object().shape({
  term: Yup.string()
    .trim('Trim')
    .required('Você precisa de adicionar os seus próprios termos'),
});

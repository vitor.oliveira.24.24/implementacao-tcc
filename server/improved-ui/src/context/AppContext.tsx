import React, { createContext, useReducer } from 'react';
import AppReducer from '../reducers/AppReducer';

export interface Term {
  id: string;
  name: string;
}

export interface GlobalContextProps {
  terms: Term[];
  addTerm?: (term: Term) => void;
  delTerm?: (id: string) => void;
}

const initialValues: GlobalContextProps = {
  terms: [],
};

export const AppContext = createContext<GlobalContextProps>(initialValues);

export const AppProvider: React.FC = ({ children }) => {
  const [{ terms }, dispatch] = useReducer(AppReducer, initialValues);

  const addTerm = (term: Term) => {
    dispatch({ type: 'ADD_TERM', payload: term });
  };

  const delTerm = (id: string) => {
    dispatch({ type: 'DEL_TERM', payload: id });
  };

  return (
    <AppContext.Provider value={{ terms, addTerm, delTerm }}>
      {children}
    </AppContext.Provider>
  );
};

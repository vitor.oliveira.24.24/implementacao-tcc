import React, { createContext, useReducer } from 'react';
import ConfigReducer from '../reducers/ConfigReducer';

export interface ToolConfig {
  lang: string;
  pages: number;
}

export interface ConfigContextProps {
  config: ToolConfig;
  updateConfig?: (newConfig: ToolConfig) => void;
}

const initialValues: ConfigContextProps = {
  config: { lang: 'english', pages: 0 },
};

export const ConfigContext = createContext<ConfigContextProps>(initialValues);

export const ConfigProvider: React.FC = ({ children }) => {
  const [{ config }, dispatch] = useReducer(ConfigReducer, initialValues);

  const updateConfig = (newConfig: ToolConfig) =>
    dispatch({ type: 'UPDATE_CONFIG', payload: newConfig });

  return (
    <ConfigContext.Provider value={{ config, updateConfig }}>
      {children}
    </ConfigContext.Provider>
  );
};

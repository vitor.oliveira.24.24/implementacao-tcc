import { GlobalContextProps, Term } from '../context/AppContext';

type Action =
  | {
      type: 'ADD_TERM';
      payload: Term;
    }
  | {
      type: 'DEL_TERM';
      payload: string;
    };

type State = GlobalContextProps;

const AppReducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'ADD_TERM':
      return {
        ...state,
        terms: [...state.terms, action.payload],
      };
    case 'DEL_TERM':
      return {
        ...state,
        terms: state.terms.filter(({ id }) => id !== action.payload),
      };
    default:
      return state;
  }
};

export default AppReducer;

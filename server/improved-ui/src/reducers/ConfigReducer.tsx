import { ConfigContextProps, ToolConfig } from '../context/ConfigContext';

type Action = {
  type: 'UPDATE_CONFIG';
  payload: ToolConfig;
};

type State = ConfigContextProps;

const ConfigReducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'UPDATE_CONFIG':
      return {
        ...state,
        config: action.payload,
      };
    default:
      return state;
  }
};

export default ConfigReducer;

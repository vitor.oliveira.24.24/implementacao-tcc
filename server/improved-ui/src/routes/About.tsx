import React from 'react';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import Description from '@material-ui/icons/Description';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import uenpLogo from '../sponsors/uenp-logo.jpg';
import fundAraucariaLogo from '../sponsors/fundacao-araucaria.jpg';

const aboutStyles = makeStyles((theme: Theme) =>
  createStyles({
    largeAvatar: {
      width: theme.spacing(15),
      height: theme.spacing(15),
    },
    divMargins: {
      marginTop: '2%',
    },
  })
);

const About: React.FC = () => {
  const { largeAvatar, divMargins } = aboutStyles();
  return (
    <div>
      <div className={divMargins}>
        <Typography variant='h4' align='center' gutterBottom>
          <span>
            <Description />
          </span>{' '}
          Descrição
        </Typography>
        <Typography align='justify' variant='h5' paragraph>
          Ferramenta desenvolvida em conjunto pelo professor Dr. André Menolli e
          pelo orientando Vitor Oliveira, como parte do projeto de Iniciação
          Ciêntífica apoiado pela Universidade Estadual do Norte do Paraná e
          patrocinada pela Fundação Araucária.
        </Typography>
      </div>
      <div className={divMargins}>
        <Typography align='center' variant='h4'>
          Patrocinadores
        </Typography>
        <Grid
          className={divMargins}
          container
          spacing={5}
          alignItems='center'
          justify='center'>
          <Grid item>
            <Link
              href='https://uenp.edu.br'
              target='_blank'
              rel='noopener noreferrer'>
              <Avatar className={largeAvatar} alt='uenp' src={uenpLogo} />
            </Link>
          </Grid>
          <Grid item>
            <Link
              href='https://www.fappr.pr.gov.br'
              target='_blank'
              rel='noopener noreferrer'>
              <Avatar
                className={largeAvatar}
                alt='fundação auraucária'
                src={fundAraucariaLogo}
              />
            </Link>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};

export default About;

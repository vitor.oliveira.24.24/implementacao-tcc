import React from 'react';
import TermList from '../components/layout/TermList';
import TermForm from '../components/form-components/TermForm';

const MainRoute: React.FC = () => {
  return (
    <>
      <TermForm />
      <TermList />
    </>
  );
};

export default MainRoute;

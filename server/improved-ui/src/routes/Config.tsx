import React from 'react';
import ConfigForm from '../components/form-components/ConfigForm';

const Config: React.FC = () => {
  return <ConfigForm />;
};

export default Config;

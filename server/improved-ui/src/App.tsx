import React from 'react';
import AppRouter from './components/layout/AppRouter';
import { AppProvider } from './context/AppContext';
import { ConfigProvider } from './context/ConfigContext';

const App: React.FC = () => {
  return (
    <ConfigProvider>
      <AppProvider>
        <AppRouter />
      </AppProvider>
    </ConfigProvider>
  );
};

export default App;

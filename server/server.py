from time import perf_counter

from flask import Flask, make_response, request

from backend import Search

app = Flask(__name__)


@app.route('/search', methods=['POST'])
def search_route():
    if request.method == 'POST':
        req = request.get_json()
        search = Search.Search(req['termList'], req['pages'], req['lang'])
        start = perf_counter()
        search.startAsyncSearch()
        print('Took', perf_counter() - start, 'seconds.')
        return make_response(search.data, 200)

    return make_response('request error', 404)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=True)

from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support.ui import WebDriverWait


class SeleniumConnection(object):
    @staticmethod
    def create_webdriver():
        opt = Options()
        opt.add_argument('--headless')
        opt.add_argument('--safe-mode')
        opt.add_argument('-private-window')

        return Firefox(options=opt)

    @staticmethod
    def create_wait(driver, timeout: int = 5):
        return WebDriverWait(driver, timeout)

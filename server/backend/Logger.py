import logging


class Logger(object):
    def __init__(self):
        self.__logger = logging.getLogger()
        logging.basicConfig(format='%(asctime)s - %(message)s',
                            datefmt='%d-%b-%y %H:%M:%S')

    def logError(self, msg: str):
        self.__logger.error(msg)

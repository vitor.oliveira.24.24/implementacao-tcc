from typing import List
from urllib.parse import quote


class Acm(object):
    def __init__(self, terms: List[str], pages: int):
        self.__terms = terms
        self.__pages = pages
        self.__pageSize = 50
        self.__base = 'https://dl.acm.org/action/doSearch?fillQuickSearch=false&expand=dl'

    def __formatAcm(self):
        for term in self.__terms:
            split = term.replace(' ', '+')
            query = f'&field1=AllField&text1={quote(split, safe="+")}'
            yield query

    def generateUrls(self) -> List[str]:
        individualACMQueries = []

        for urlPiece in self.__formatAcm():
            for page in range(self.__pages + 1):
                individualACMQueries.append(
                    self.__base + urlPiece +
                    f'&startPage={page}&pageSize={self.__pageSize}&ContentItemType=research-article'
                )

        return individualACMQueries

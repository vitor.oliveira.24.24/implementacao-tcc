from typing import List, Tuple

from backend.Acm import Acm
from backend.Ieee import Ieee
from backend.ScienceDirect import ScienceDirect


class Domain(object):
    def __init__(self, args: list, pages: int):
        self.__base = ['sdirect', 'ieee', 'acm']
        self.__args = args
        self.__pages = pages
        self.__urls = []

    @property
    def urls(self) -> List[Tuple[str, str]]:
        return self.__urls

    def parseArgs(self):
        for base in self.__base:
            if base == 'acm':
                acm = Acm(self.__args, self.__pages)
                urls = acm.generateUrls()
                self.__urls.append((base, urls))
            elif base == 'sdirect':
                sdirect = ScienceDirect(self.__args, self.__pages)
                urls = sdirect.generateUrls()
                self.__urls.append((base, urls))
            elif base == 'ieee':
                ieee = Ieee(self.__args, self.__pages)
                urls = ieee.generateUrls()
                self.__urls.append((base, urls))
            else:
                raise Exception('Base inexistente')

from typing import List, Tuple

import aiohttp
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import \
    presence_of_element_located

from backend.SeleniumConnection import SeleniumConnection


class Connection(object):
    def __init__(self, queries: List[Tuple[str, str]]):
        self.__queries = queries
        self.__driver = SeleniumConnection.create_webdriver()
        self.__wait = SeleniumConnection.create_wait(self.__driver, 20)

    async def connect(self):
        for base, urls in self.__queries:
            if base == 'ieee':
                for url in urls:
                    self.__driver.get(url)
                    self.__wait.until(presence_of_element_located(
                        (By.CLASS_NAME, 'List-results-items')))
                    yield base, url, self.__driver.page_source
            elif base == 'sdirect':
                for url in urls:
                    self.__driver.get(url)
                    self.__wait.until(presence_of_element_located((By.ID, 'srp-results-list')))
                    yield base, url, self.__driver.page_source
            else:
                for url in urls:
                    async with aiohttp.ClientSession() as session:
                        async with session.get(url) as res:
                            body = await res.text()
                            yield base, url, body

        self.__driver.close()

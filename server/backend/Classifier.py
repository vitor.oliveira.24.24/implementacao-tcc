from collections import Counter


class Classifier(object):
    def __init__(self, originalTerms: list):
        self.__limit = 10
        self.__frequencyMap = Counter()
        self.__weightMap = {}
        self.__originalTerms = originalTerms

    @property
    def frequencyData(self) -> list:
        res = dict(filter(lambda kv: kv[0] not in self.__originalTerms,
                          self.__frequencyMap.items()))

        return sorted(res.items(), key=lambda kv: kv[1], reverse=True)[:self.__limit]

    @property
    def weightData(self) -> list:
        return sorted(filter(lambda kval: kval[0] not in self.__originalTerms,
                             self.__weightMap.items()),
                      key=lambda kv: kv[1], reverse=True)[:self.__limit]

    def combine_recommendations(self):
        arr = self.frequencyData + self.weightData
        str_arr = list(map(lambda kv: kv[0], arr))
        return list(set(str_arr))

    async def frequencyClassifier(self, termList: list):
        print('[Freq classifier] classifying')

        for obj in termList:
            title, abstract, keywords = obj.get('title', []), obj.get(
                'abstract', []), obj.get('keywords', [])
            self.__frequencyMap.update(title + abstract + keywords)

        print('[Freq classifier] end classification')

    async def weightClassifier(self, termList: list):
        print('[Weight classifier] classifying')

        # 1 titulo, 0.5 abstract e 1 keywords
        for obj in termList:
            title, abstract, keywords = obj.get('title', []), obj.get(
                'abstract', []), obj.get('keywords', [])

            for word in set(title + abstract + keywords):
                score = 0

                if title and word in title:
                    score += title.count(word)
                if abstract and word in abstract:
                    score += (0.5 * abstract.count(word))
                if keywords and word in keywords:
                    score += keywords.count(word)

                score /= 2.5

                if self.__weightMap.get(word, None):
                    self.__weightMap[word] += score
                else:
                    self.__weightMap[word] = score

        print('[Weight classifier] end classification')

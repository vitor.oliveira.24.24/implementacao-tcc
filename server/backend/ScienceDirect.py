import urllib.parse
from typing import List


class ScienceDirect(object):
    def __init__(self, terms: List[str], pages: int = 0):
        self.__terms = terms
        self.__pageSize = 50
        self.__pages = pages
        self.__base = 'https://www.sciencedirect.com'

    def __formatSD(self):
        for term in self.__terms:
            yield term

    def generateUrls(self) -> List[str]:
        individualSdirectQueries = []

        for urlPiece in self.__formatSD():
            for page in range(self.__pages + 1):
                piece = f'/search/advanced?qs=({urllib.parse.quote(urlPiece)})&articleTypes=FLA&show={self.__pageSize}&sortBy=relevance&offset={page * self.__pageSize}'
                individualSdirectQueries.append(self.__base + piece)

        return individualSdirectQueries

import json


class Encoder(object):
    def __init__(self, content: dict, encoding: str):
        self.__content = content
        self.__encoding = encoding.lower()
        self.__operations = {
            'json': self.__encodeJson
        }

    def encode(self) -> str:
        operation = self.__operations.get(self.__encoding, None)

        if operation:
            return operation()
        return ''

    def __encodeJson(self) -> str:
        return json.dumps(self.__content, ensure_ascii=True, indent=None)

from typing import List
from urllib.parse import quote

'''
https://ieeexplore.ieee.org/search/searchresult.jsp?action=search&newsearch=true&matchBoolean=true&queryText=(%22Full%20Text%20.AND.%20Metadata%22:Bloom%E2%80%99s%20taxonomy)%20AND%20(%22Full%20Text%20.AND.%20Metadata%22:Bloom%20taxonomy)%20AND%20(%22Full%20Text%20.AND.%20Metadata%22:cognitive%20taxonomy)%20AND%20(%22Full%20Text%20.AND.%20Metadata%22:programming)%20AND%20(%22Full%20Text%20.AND.%20Metadata%22:computer%20science)

'''


class Ieee(object):
    def __init__(self, terms: List[str], pages: int):
        self.__terms = terms
        self.__pages = pages
        self.__pageSize = 50
        self.__base = 'https://ieeexplore.ieee.org/search/searchresult.jsp?action=search&newsearch=true&matchBoolean' \
                      '=true&queryText='
        self.__end = f'&highlight=true&returnFacets=ALL&returnType=SEARCH&matchPubs=true&rowsPerPage={self.__pageSize}'

    def __formatIeee(self) -> str:
        for i, term in enumerate(self.__terms):
            query = ''
            query_type = quote('"Full Text .AND. Metadata"')
            query += f'({query_type}:{quote(term)})'
            yield query

    def generateUrls(self) -> List[str]:
        individualIeeeQueries = []

        for query in self.__formatIeee():
            for page in range(self.__pages + 1):
                individualIeeeQueries.append(
                    self.__base + query + self.__end + f'&pageNumber={page + 1}')

        return individualIeeeQueries

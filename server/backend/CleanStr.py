import string

from nltk.corpus import stopwords
from nltk.stem import PorterStemmer
from nltk.tag import pos_tag
from nltk.tokenize import word_tokenize


class CleanStr(object):
    def __init__(self, lang: str):
        self.__stopwords = stopwords.words(lang)
        self.__nouns = ['NN', 'NNS', 'NNP']
        # self.__verbs = ['VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']

    def __cleanWords(self, word: str) -> str:
        return ''.join(list(filter(lambda char:
                                   char not in string.punctuation, word))).lower()

    def __cleanSentence(self, sentence: str) -> str:
        return ' '.join(list(filter(lambda word:
                                    word not in self.__stopwords, sentence))).lower()

    def __filterWord(self, word: str, pos: str) -> str:
        if pos in self.__nouns:
            return word
        return ''

    # https://stackoverflow.com/a/33587889
    def extractTokens(self, sentence: str) -> list:
        tokenized = word_tokenize(sentence)
        return list(filter(lambda term: term != '',
                           [self.__filterWord(word, pos) for (word, pos)
                               in pos_tag(tokenized)]))

    # https://riptutorial.com/nltk/example/27393/porter-stemmer
    # https://www.nltk.org/api/nltk.stem.html
    def __extractWordSteam(self, word: str) -> str:
        stemmer = PorterStemmer()
        return stemmer.stem(word)

    def cleanText(self, text: str) -> str:
        cleanWords = self.__cleanWords(text)
        words = cleanWords.split()
        return self.__cleanSentence(words)

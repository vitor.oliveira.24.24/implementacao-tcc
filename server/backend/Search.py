import asyncio
from typing import List

from backend.Classifier import Classifier
from backend.Connection import Connection
from backend.Domain import Domain
from backend.Encoder import Encoder
from backend.GenericParser import GenericParser


class Search(object):
    def __init__(self, terms: List[str], pages: int, lang: str):
        self.__terms = terms
        self.__pages = pages
        self.__encodedRes = ''
        self.__lang = lang

    @property
    def data(self) -> str:
        return self.__encodedRes

    def __createConnectionGenerator(self):
        domain: Domain = Domain(self.__terms, self.__pages)
        domain.parseArgs()
        connection: Connection = Connection(domain.urls)
        return connection.connect()

    async def __main(self, connGenerator):
        genericParser = GenericParser(connGenerator, self.__lang)

        classifier = Classifier(self.__terms)

        async for termList in genericParser.parseBase():
            await classifier.frequencyClassifier(termList)
            await classifier.weightClassifier(termList)

        encoder = Encoder({
            'freq_class': classifier.frequencyData,
            'weight_class': classifier.weightData,
            'recommendations': classifier.combine_recommendations()
        }, 'json')

        self.__encodedRes = encoder.encode()

    def startAsyncSearch(self):
        c_loop = asyncio.new_event_loop()
        c_loop.run_until_complete(self.__main(self.__createConnectionGenerator()))

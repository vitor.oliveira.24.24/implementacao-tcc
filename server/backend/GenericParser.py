import json
from typing import Tuple, List

from bs4 import BeautifulSoup
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.expected_conditions import \
    presence_of_element_located
from selenium.webdriver.support.expected_conditions import \
    visibility_of_all_elements_located

from backend.CleanStr import CleanStr
from backend.SeleniumConnection import SeleniumConnection


class GenericParser(object):
    def __init__(self, contentGenerator, lang: str):
        self.__contentGenerator = contentGenerator
        self.__availableBases = {
            'acm': self.__acm_parsing,
            'sdirect': self.__sdirect_parsing,
            'ieee': self.__ieee_parsing
        }
        self.__driver = SeleniumConnection.create_webdriver()
        self.__cleaner = CleanStr(lang)

    async def parseBase(self):
        async for (base, url, rawPage) in self.__contentGenerator:
            parseFunc = self.__availableBases.get(base)

            if parseFunc:
                print(f'[{base}] Running {url}')
                content = await parseFunc(rawPage)
                print(f'[{base}] Extracted {url}')
                yield content

        self.__driver.close()

    async def __acm_parsing(self, res: str) -> list:
        soup = BeautifulSoup(res, 'lxml')
        final = []

        for result in soup.findAll('div', {'class': 'issue-item__content'}):
            output = {}
            output['title'] = self.__cleaner.extractTokens(
                self.__cleaner.cleanText(
                    result.find(
                        'h5', {'class': 'issue-item__title'}).getText().strip()))
            abstractDiv = result.find(
                'div', {'class': 'abstract-text'})

            if abstractDiv:
                abstractText = abstractDiv.find('p')
                output['abstract'] = self.__cleaner.extractTokens(
                    self.__cleaner.cleanText(
                        abstractText.getText().strip()))

            keywords = result.find('div', {'class': 'keywords-text'})
            output['keywords'] = ''

            if keywords:
                output['keywords'] += keywords.find(
                    'p').getText().strip() + " "

            subject = result.find('div', {'class': 'Subject'})

            if subject:
                for sub in subject.findAll('p'):
                    output['keywords'] += sub.getText().strip() + " "

            output['keywords'] = self.__cleaner.cleanText(
                output['keywords']).split()
            final.append(output)

        return final

    def __sdirect_metadata_parser(self, res: str) -> Tuple[str, List[str]]:
        soup = BeautifulSoup(res, 'lxml')

        try:
            abstract = soup.find('div', {'class': 'abstract author'}).find('p').getText()
        except AttributeError:
            abstract = ''

        try:
            keywords = [
                keyword.find('span').getText()
                for keyword in soup.find('div', {'class': 'keywords-section'}).findAll('div', {'class': 'keyword'})]
        except AttributeError:
            keywords = []

        return abstract, keywords

    async def __sdirect_parsing(self, res: str) -> list:
        wait = SeleniumConnection.create_wait(self.__driver, 0)
        base = 'https://www.sciencedirect.com'
        soup = BeautifulSoup(res, 'lxml')
        articles = soup.find('div', id='srp-results-list').find('ol', {'class': 'search-result-wrapper'}).findAll('li',
                                                                                                                  {
                                                                                                                      'class': 'ResultItem col-xs-24 push-m'})
        final = []

        for article in articles:
            article_url = article.find('a', {'class': 'result-list-title-link u-font-serif text-s'})
            title, url = article_url.getText(), article_url['href']
            print(f'[Sdirect] Extracting {url}')

            try:
                self.__driver.get(base + url)
                wait.until(presence_of_element_located((By.CLASS_NAME, 'keywords-section')))
            except TimeoutException:
                try:
                    self.__driver.get(base + url)
                    wait.until(presence_of_element_located((By.ID, 'abstracts')))
                except TimeoutException:
                    continue

            abstract, keywords = self.__sdirect_metadata_parser(self.__driver.page_source)

            final.append({
                'title': self.__cleaner.extractTokens(self.__cleaner.cleanText(title.strip())),
                'abstract': self.__cleaner.extractTokens(self.__cleaner.cleanText(abstract.strip())),
                'keywords': self.__cleaner.extractTokens(self.__cleaner.cleanText(' '.join(keywords).strip()))
            })

        return final

    async def __ieee_parsing(self, res: str):
        wait = SeleniumConnection.create_wait(self.__driver, 50)
        soup = BeautifulSoup(res, 'lxml')
        results = soup.find_all('div', {'class': 'col result-item-align'})
        final = []

        for result in results:
            output = {}
            url = result.find('h2').find('a')['href']

            if not url.startswith('/document'):
                continue

            print('fetch url', 'https://ieeexplore.ieee.org' +
                  url + 'keywords#keywords')
            self.__driver.get('https://ieeexplore.ieee.org' + url)
            wait.until(visibility_of_all_elements_located(
                (By.XPATH, '//*[@id="keywords"]')))

            elem = self.__driver.find_element_by_css_selector(
                '#keywords-header > div:nth-child(2) > i:nth-child(1)')

            self.__driver.execute_script('arguments[0].click()', elem)
            soup2 = BeautifulSoup(self.__driver.page_source, 'lxml')
            title = soup2.find('h1', {'class': 'document-title'}).getText()

            try:
                abstract = soup2.find('section', {
                    'class': 'document-abstract'}).find('div', {'class': 'u-mb-1'}).find('div').getText()
            except AttributeError:
                abstract = ''

            try:
                keywords = []

                for item in soup2.find('div', id='keywords').find(
                        'div', {'class': 'stats-keywords-container'}).find('ul', {
                    'class': 'doc-keywords-list stats-keywords-list'}).find_all('li',
                                                                                {'class': 'doc-keywords-list-item'}):
                    items = item.find_all(
                        'a', {'class': 'stats-keywords-list-item'})

                    for keyword in items:
                        kw = json.loads(keyword['data-tealium_data'])
                        keywords.append(kw['keyword'])
            except AttributeError:
                keywords = []

            output['title'] = self.__cleaner.extractTokens(
                self.__cleaner.cleanText(title.strip()))
            output['abstract'] = self.__cleaner.extractTokens(
                self.__cleaner.cleanText(abstract.strip()))
            output['keywords'] = self.__cleaner.extractTokens(
                self.__cleaner.cleanText(' '.join(keywords).strip()))
            final.append(output)

        return final

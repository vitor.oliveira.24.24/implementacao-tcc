class GooglePatentes(object):
    def __init__(self, args: list):
        self.__args: list = args
        self.__url: str = 'https://patents.google.com/xhr/query?url=q%3D' 
    
    def setArgs(self, args: list):
        self.__args = args
    
    def getArgs(self) -> list:
        return self.__args

    def getUrl(self) -> str:
        return self.__url
    
    def __formatGooglePatents(self) -> str:
        queryString: str = ""
        for arg in self.__args:
            queryString += '(' + arg + ')'
        return queryString + "&exp="
    def generateUrl(self) -> str:
        # formatar essa url com os parâmetros do usuário
        self.__url += self.__formatGooglePatents()
        print ("Google patentes:", self.__url)
# Install Firefox Webdriver

This project uses the Selenium framework in order to scrap dynamic webpages.

This project uses the Firefox browser, so, you will need the Firefox webdriver.

## Installation

- Download the firefox webdriver: `wget https://github.com/mozilla/geckodriver/releases/download/v0.29.0/geckodriver-v0.29.0-linux64.tar.gz`.

- Extract the tar file with `tar -xvf geckodriver-v0.29.0-linux64.tar.gz`.

- Finally, put it on the system path (Linux systems): `sudo mv geckodriver /usr/local/bin`.